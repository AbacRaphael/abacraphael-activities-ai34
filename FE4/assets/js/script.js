var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
    datasets: [
    {
      label: 'Borrowed Books',
      backgroundColor: 'rgb(255,0,0)',
    },
    {
      label: 'Returned Books',
      backgroundColor: 'rgba(0,128,0)',
    }
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});

var zxy = document.getElementById('barChart_1').getContext('2d');
var myChart1 = new Chart(zxy, {
  type: 'bar',
  data: {
    labels: ['Computer Science','History'],
    datasets: [
    {
      label: 'Most Picked Category',
      backgroundColor: 'rgb(255,182,193)',
    },
  ],
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});