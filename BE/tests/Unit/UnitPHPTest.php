<?php

namespace Tests\Unit;
use App\Models\categories;
use App\Models\books;
use App\Models\borrowed_books;
use App\Models\returned_books;
use App\Models\patrons; 
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ModelRelationshipTest extends TestCase
{

    use RefreshDatabase; 
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function test_book_to_category() {
        $category = categories::create(['category' => 'Computer']);
        $book = books::factory()->create(['category_id' => $category->id]);

        $this->assertInstanceOf(categories::class, $book->category);
    }

    public function test_category_many_books() {
        $category = categories::create(['category' => 'Computer']);
        $book = books::factory()->create(['category_id' => $category->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $category-books);
    }
    
    
    public function test_book_many_borrowed() {
        $category = categories::create(['category' => 'Science']);
        $book = books::factory()->create(['category_id' => $category->id]);
        $patron = patrons::factory()->create();
        $borrowedbooks = borrowed_books::create([
            'patron_id' => $patron->id,
            'book_id' => $book->id,
            'copies' => rand(1,10)
        ]);
        $this->assertTrue($book->returned->contains($borrowedbooks));
    }

    public function test_book_many_returned() {
        $category = categories::create(['category' => 'Science']);
        $book = books::factory()->create(['category_id' => $category->id]);
        $patron = patrons::factory()->create(['category_id' => $category->id]);
        $returnedbooks = returned_books::create([
            'patron_id' => $patron->id,
            'book_id' => $book->id,
            'copies' => rand(1, 10)
        ]);
        $this->assertTrue($book->returned->contains($returnedbooks));
    }

    public function test_borrowed_to_books() {
        $category = categories::create(['category' => 'Science']);
        $book = books::factory()->create(['category_id' => $category->id]);
        $patron = patrons::factory()->create();
        $borrowedbooks = borrowed_books::create([
            'patron_id' => $patron->id,
            'book_id' => $book->id,
            'copies' => rand(1,10)
        ]);
        $this->assertTrue($book->returned->contains($borrowedbooks));
    }

    public function test_borrowed_to_patrons() {
        $category = categories::create(['category' => 'Science']);
        $book = books::factory()->create(['category_id' => $category->id]);
        $patron = patrons::factory()->create(['category_id' => $category->id]);
        $returnedbooks = returned_books::create([
            'patron_id' => $patron->id,
            'book_id' => $book->id,
            'copies' => rand(1, 10)
        ]);
        $this->assertTrue($patron->returned->contains($returnedbooks));
    }

    
    public function test_returned_to_books() {
        $category = categories::create(['category' => 'Science']);
        $book = books::factory()->create(['category_id' => $category->id]);
        $patron = patrons::factory()->create();
        $borrowedbooks = borrowed_books::create([
            'patron_id' => $patron->id,
            'book_id' => $book->id,
            'copies' => rand(1,10)
        ]);
        $this->assertTrue($book->returned->contains($borrowedbooks));
    }

    public function test_returned_to_patrons() {
        $category = categories::create(['category' => 'Science']);
        $book = books::factory()->create(['category_id' => $category->id]);
        $patron = patrons::factory()->create(['category_id' => $category->id]);
        $returnedbooks = returned_books::create([
            'patron_id' => $patron->id,
            'book_id' => $book->id,
            'copies' => rand(1, 10)
        ]);
        $this->assertTrue($patron->returned->contains($returnedbooks));
    }

    
    public function test_patron_many_borrowed() {
        $category = categories::create(['category' => 'Science']);
        $book = books::factory()->create(['category_id' => $category->id]);
        $patron = patrons::factory()->create();
        $borrowedbooks = borrowed_books::create([
            'patron_id' => $patron->id,
            'book_id' => $book->id,
            'copies' => rand(1,10)
        ]);
        $this->assertTrue($patron->returned->contains($borrowedbooks));
    }

    public function test_patron_many_returned() {
        $category = categories::create(['category' => 'Science']);
        $book = books::factory()->create(['category_id' => $category->id]);
        $patron = patrons::factory()->create(['category_id' => $category->id]);
        $returnedbooks = returned_books::create([
            'patron_id' => $patron->id,
            'book_id' => $book->id,
            'copies' => rand(1, 10)
        ]);
        $this->assertTrue($patron->returned->contains($returnedbooks));
    }
}
