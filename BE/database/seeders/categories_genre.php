<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class categories_genre extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['categories' => 'Computer'],
            ['categories' => 'Science'],
            ['categories' => 'Technology'],
        ];
        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
