<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\books_controller;
use App\Http\Controllers\patron_controller;
use App\Http\Controllers\categories_controller;
use App\Http\Controllers\returnedbooks_controller;
use App\Http\Controllers\borrowedbooks_controller;
use App\Http\Controllers\books;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('books', books_controller::class);
Route::resource('patrons', patrons_controller::class);

Route::get('/borrowedbooks', borrowedbooks_controller::class)->only(['index']);
Route::post('/borrowedbooks',borrowedbooks_controller::class)->only(['store']);

Route::get('/returnedbooks',returnedbooks_controller::class)->only(['index']);
Route::post('/returnedbooks',returnedbooks_controller::class)->only(['store']);

Route::get('/categories',categories_controller::class)->only(['index']);
