<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\borrowed_books;

class borrowedbooks_controller extends Controller
{
    public function index()
    {
        return response()->json(borrowed_books::with([
            'book','patron','category'
        ])->get());
    }
    
    public function store(Request $request)
    {
        $save = borrowed_books::create($request->only([
            'book_id','copies','patron_id'
        ]));
        $borrowedbooks = borrowed_books::with([
            'book'
            ])->find($save->id);
        
        $copies = $borrowedbooks->book->copies - $request->copies;
        $borrowedbooks->book->update([
            'copies' => $copies
        ]);

        return response()->json([201, $borrowedbooks]);
    }
}
