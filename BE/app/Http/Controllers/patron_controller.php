<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\patrons;

class patron_controller extends Controller
{
    public function index()
    {
        return response()->json(patrons::with([
            'category:id,category'
            ])->get());
    }
 
    public function show($id)
    {
        $patron = patron::with([
            'category:id,category'
            ])->where('id',$id)->firstOrFail();
        return response()->json($patrons);
    }

    public function store(Request $request)
    {
        $patron = patron::create($request->all());
        return response()->json(['message' => 'Created', 'patron' =>$patron]);
    }

    public function update(Request $request, $id)
    {
        $patron = patron::with(['category:id,category'])->where('id',$id)->firstOrFail();
        $patron->update($request->all());
        return response()->json(['message' => 'Updated', 'patron' =>$patron]);
    }

    public function delete($id)
    {
        $patron = patron::where('id',$id)->firstOrFail();
        $patron->delete();
        return response()->json(['message' => 'Deleted', 'patron' =>$patron]);
    }
}
