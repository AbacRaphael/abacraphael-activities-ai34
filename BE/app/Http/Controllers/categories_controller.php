<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\categories;

class categories_controller extends Controller
{
    public function index()
    {
        return response()->json(categories::all());
    }
}
