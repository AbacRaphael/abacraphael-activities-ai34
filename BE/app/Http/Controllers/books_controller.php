<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\books;

class books_controller extends Controller
{
    public function index()
    {
        return response()->json(books::with([
            'category:id,category'
            ])->get());
    }
 
    public function show($id)
    {
        $book = book::with([
            'category:id,category'
            ])->where('id',$id)->firstOrFail();
        return response()->json($books);
    }

    public function store(Request $request)
    {
        $stored = book::create($request->validated());
        $book = book::with(['category:id,category'])->find($stored->id);
        return response()->json(201, $book);
    }

    public function update(Request $request, $id)
    {
        $book = book::with(['category:id,category'])->where('id',$id)->firstOrFail();
        $book->update($request->validated());
        $Updatebook = book::with(['category:id,category'])->where('id',$id)->firstOrFail();
        return response()->json([200, $Updatebook]);
    }

    public function delete($id)
    {
        $book = book::where('id',$id)->firstOrFail();
        $book->delete();
        return response()->json([404, $book]);
    }
}
