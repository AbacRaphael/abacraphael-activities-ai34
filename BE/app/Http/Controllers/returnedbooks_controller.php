<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\returned_books;

class returnedbooks_controller extends Controller
{
    public function index()
    {
        return response()->json(returned_books::with([
            'book','patron','category'
        ])->get());
        
        return response()->json([$returnedbooks]);
    }
    public function store(Request $request)
    {
        $save = returned_books::create($request->only([
            'book_id','copies','patron_id'
        ]));
        $returnedbooks = returned_books::with([
            'book'
            ])->find($save->id);
        
        $copies = $returnedbooks->book->copies - $request->copies;
        $returnedbooks->book->update([
            'copies' => $copies
        ]);

        return response()->json([201, $returnedbooks]);  
    }
}
