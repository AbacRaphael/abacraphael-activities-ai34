<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Patron_Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => 'required|max:100',
            'first_name' => 'required|max:100',
            'middle_name' => 'required|max:100',
            'email' => 'required|unique:patrons|email'   
        ];
    }
    
    public function messages()
    {
        return [
            'last_name.required' => 'Input Lastname',
            'first_name.required' => 'Input Firstname',
            'middle_name.required' => 'Input Middlename',
            'email.required' => 'Input Email Address',
            'email.unque' => 'Email already used.'
        ];
    }
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 400));
    }
}
