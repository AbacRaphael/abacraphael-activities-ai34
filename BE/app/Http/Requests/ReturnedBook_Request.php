<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturnedBook_Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $borrowed = borrowed_books::
            where('book_id', request()->get('book_id'))->
            where('patron_id',request()->
            get('patron_id'))->first();
        if(!empty($borrowed)) {
            $copies = $borrowed->copies;
        }
        else 
        {
            $copies = request()->get('copies');
        }

        return [
            'book_id' => 'required|exist:borrowed_books, book_id',
            'copies' => 'required|numeric',
            'patron_id' => 'exist:borrowed_books,patron_id'
        ];
    }
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 400));
    } 
}
