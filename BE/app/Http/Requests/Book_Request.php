<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Book_Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:150',
            'author' => 'required|max:150',
            'copies' => 'required|numeric',
            'category_id' => 'required|exist:categories,id'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'You forgot to input Name of the book',
            'author.required' => 'The author name is Required.',
            'copies.required' => 'Input copies of the book',
            'category_id.required' => 'Input category'
        ];
    }
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 400));
    }
}
