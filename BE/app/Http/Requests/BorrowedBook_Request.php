<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BorrowedBook_Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $book = Book::find(request()->get('book_id'));
        if(!empty($book))
        {
            $copies = $book->copies;
        }
        return [
            'patron_id' => 'required|numeric',
            'book_id' => 'required|exist:patrons,id',
            'copies' => ['required',"lte:{$copies}", 'bail','gt:0'],
        ];
    }
    public function messages()
    {
        return [
            'patron_id.exist' => 'Patron does not exist',
            'books_id.exist' => 'Book does not exist',
            'copies' => ['required',"lte:{$copies}", 'bail','gt:0']
        ];
    }
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 400));
    }
}
