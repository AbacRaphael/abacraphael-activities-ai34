<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;
    
    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function category() {
        return $this->hasOne(category::class, 'id', 'category_id');
    }

    public function returned_books() {
        return $this->hasMany(returned_books::class, 'id', 'book_id');
    }   
    public function borrowed_books() {
        return $this->hasMany(borrowed_books::class, 'id', 'book_id');
    }   
}
