<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class patrons extends Model
{
    use HasFactory;

    protected $fillable = ['fname', 'mname', 'lname', 'email', 'date'];


    public function returned_books() {
        return $this->hasMany(returned_books::class, 'id', 'patron_id');
    }   
    public function borrowed_books() {
        return $this->hasMany(borrowed_books::class, 'id', 'patron_id');
    }   
}
