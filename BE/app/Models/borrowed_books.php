<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class borrowed_books extends Model
{
    use HasFactory;

    protected $fillable = ['patron_id', 'copies', 'book_id', 'category'];

    public function patron() {
        return $this->belongsTo(patron::class, 'patron_id', 'id');
    }
    
    public function book() {
        return $this->belongsTo(book::class, 'book_id', 'id');
    }

    public function category() {
        return $this->belongsTo(category::class, 'category', 'id');
    }
}
