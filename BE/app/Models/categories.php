<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    use HasFactory;
    
    protected $fillabe = ['category'];
    
    public function books() {
        return $this->belongsToMany(Book::class, 'categories', 'id', 'category_id');
    }
}
