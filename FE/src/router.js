import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from './pages/dashboard/dashboard';

Vue.use(VueRouter)

const routes = 
[
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
    },
    {
        path:'/Dashboard',
        component: () => import('./pages/dashboard/dashboard')
      },
      {
        path:'/Patron_management',
        component: () => import('./pages/patron/Patron_management')
      },
      {
        path:'/Book_management',
        component: () => import('./pages/book/Book_management')
      },
      {
        path:'/settings',
        component: () => import('./pages/settings/settings')
      },
]

const router = new VueRouter({
  routes
})

export default router
