import Vue from 'vue'
import app from './router/app'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import store from './store';

// Install BootstrapVue
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)


Vue.config.productionTip = false
Vue.use(VueToast, {
  duration: 3000
})

new Vue({
  router,store,
  render: function (h) {  
    return h(app)},
}).$mount('#app')
