import vue from 'vue';
import Vuex from 'vuex';

vue.use(Vuex);

import book from './modules/book';
import borrowed from './modules/borrowed';
import patron from './modules/patron';
import returned from './modules/returned';

export default new Vuex.Store({
    modules:{
        book,
        borrowed,
        patron,
        returned
    }
})


