import vue from 'vue';
import Vuex from 'vuex';
import Axios from '../../base/config'

vue.use(Vuex);

const borrowedbook = 'borrowedbook'

export default ({
    namespaced: true,
    state: {

    },
    actions: {
        async storeBorrowed({commit}, {index, data}){
            const res = await Axios.post(`${borrowedbook}`, data).then(response => {
                commit('books/UPDATE_COPIES', {id: index, data: reponse.data.borrowedbook.book.copies}, {root: true})
                return response
            }).catch(error => {
                return error.response
            })
            return res;
        }
    }
})