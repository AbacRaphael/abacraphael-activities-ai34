import vue from 'vue';
import Vuex from 'vuex';
import Axios from '../../base/config'

vue.use(Vuex);

const returnedbook = 'returnedbook'

export default ({
    namespaced: true,
    state: {

    },
    actions: {
        async createReturned({commit}, {index, data}){
            const res = await Axios.post(`${returnedbook}`, data).then(response => {
                commit('books/UPDATE_COPIES', {id: index, data: reponse.data.returnedbook.book.copies}, {root: true})
                return response
            }).catch(error => {
                return error.response
            })
            return res;
        }
    }
})