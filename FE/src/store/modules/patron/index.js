import vue from 'vue';
import Vuex from 'vuex';
import Axios from '../../base/config'

vue.use(Vuex);


const PATRON = 'patron'


export default ( {
    namespaced: true,
    state: {
        patron: []
    },
    actions: {
        async getPatron({commit}){
            const res = await Axios.get(PATRON).then(response => {
                commit('SET_PATRON', response.data)
                return response
            }).catch(error => {
                return error.response
            });
            return res;
        },
        async storePatron({commit}, data){
            const res = await Axios.post(`${PATRON}`, data).then(response => {
                commit('SAVE_PATRON', response.data.patron)
                return response
            }).catch(error => {
                return error.response
            });
            return res;
        },
        async deletePatron({commit}, id){
            const res = await Axios.delete(`${PATRON}/${id}`).then(response => {
                commit('DELETE_PATRON', id);
                return response;
            });
            return res;
        },
        async updatePatron({commit}, {index, data}){
            const res = await Axios.put(`${PATRON}/${data.id}`, data).then(response => {
                commit('UPDATE_PATRON', {index, data});
                return response
            }).catch(error => {
                return error.response
            });
            return res;
        }
    },
    getters: {
        getPatronData: function (state){
            return state.patron;
        }
    },
    mutations: {
        SET_PATRON(state, data){
            state.patron = data;
        },
        SAVE_PATRON(state, data){
            state.patron.unshift({
                id: data.id,
                firstname: data.firstname,
                middlename: data.middlename,
                lastname: data.lastname,
                email: data.emailAdd,
                created_at: data.created_at
            });
        },
        UPDATE_PATRON(state, {index, data}) {
            vue.set(state.patron, index, data)
        },
        DELETE_PATRON(state, id){
            state.patron = state.patron.filter(patron => {
                return patron.id !==id
            });
        }
    },
})